# tk-shotgun-mno

tk-shotgun-mno allows you to submit PlaylistVersionConnection, basically Version entities connected to Playlist entities, entities from Shotgun Studio to Deadline's MNO, Multiple Nuke Outputs, Plugin.

## Overview
![tk-shotgun-mno flow diagram](docs/images/tk-shotgun-mno_flow_diagram.png)

tk-shotgun-mno is a tk-shotgun engine app that adds a `Generate MNO Output` Menu Item to Playlist Entities in Shotgun Studio. Playlist Entities must have a field where the MNO Output name is selected and checkbox fields for each write/output they want from that MNO Output they selected. tk-shotgun-mno supports multi-select so multiple Playlist entities may be chosen.

![Shotgun Studio Playlist Menu](docs/images/shotgun_menu.png)

Once tk-shotgun-mno is launched via `Generate MNO Output`, it pulls down PlaylistVersionConnection entities. It also loads in the MNO Config file for this project to verify the user's settings on each selected playlist. Then, tk-shotgun-mno prepares the jobs for submission to Deadline.

In the meanwhile, another node is running [Deadline's Web Service](https://docs.thinkboxsoftware.com/products/deadline/10.0/1_User%20Manual/manual/web-service.html#web-service-ref-label). This web service allows tk-shotgun-mno to contact Deadline remotely through the [Standalone Deadline Python API](https://docs.thinkboxsoftware.com/products/deadline/10.0/3_Python%20Reference/). The job submitted by tk-shotgun-mno is a MNO Application Plugin.

You should see a `Command Output` overlay window with a similar looking response.

![tk-shotgun-mno Success](docs/images/tk-shotgun-mno_success.png)

## Install

### Deadline

#### Prerequisites

This assumes that Deadline has the latest Phosphene Deadline customizations, and that MNO is properly setup in Deadline and an MNO Config file for each project.

#### Create Web Service Deadline User

1. Open the Deadline Monitor
1. **Tools** > **Super User Mode**
    * Enter Password if necessary
1. **Tools** > **Manage Users**
1. Click Add
1. Enter a username in the pop-up, you can call it `webservice`
1. In the right panel go down to **Web Service Authentication Settings**
1. Enter a Web Service password.
    * Note both the username and password as you will need them when setting up tk-shotgun-mno
1. Click Ok

#### Activate Deadline Web Service

1. Open the Deadline Monitor
1. **Tools** > **Super User Mode**
    * Enter Password if necessary
1. **Tools** > **Configure Repository Options**
1. From the left pane, choose **Web Service Settings**
1. Set a **Listening Port**, 8082 is the default.
    * Note this port for tk-shotgun-mno setup and firewall settings on the node running the Deadline Web Service.
1. Check **Require Authentication**
1. Check **Allow Exection of Non-Script Commands**
1. Uncheck **Allow Empty Passwords**
1. Click Ok

#### Install Deadline Web Service as a Service
In order to make sure the node running the Deadline Web Service starts up properly, it is highly recommended to install it as a service on the local system. Please read the documentation first on [Deadline's Web Service](https://docs.thinkboxsoftware.com/products/deadline/10.0/1_User%20Manual/manual/web-service.html#web-service-ref-label) to make sure the system/platform your setting it up on is setup properly.

##### Windows
**This Requires that Deadline Repository, SGTK, and Project Directories be on UNC Paths and that Windows Logon User Have Share Access**
To install Deadline Web Service as a Windows Service, use the [NSSM](https://www.nssm.cc), Non-Sucking Service Manager.

Once you have downloaded NSSM, launch NSSM via a Command Prompt with:
```
nssm.exe install Deadline{Major_Version}WebService
```

1. Under **Application**
    1. Set Path to `C:\Program Files\Thinkbox\Deadline{Major_Version}\bin\deadlinewebservice.exe`
    1. Set Startup Directory to `C:\Program Files\Thinkbox\Deadline{Major_Version}\bin`
1. Under `Details`
    1. Set Display Name to `Deadline{Major_Version}WebService`
    1. Startup Type to `Automatic(Delayed Start)`
1. Click **Install Service**

### Shotgun Toolkit

#### Add to Templates
The following keys must exist under `templates.yml` in order to use them in tk-shotgun-mno's app settings.

Under `keys` in `templates.yml`, add `version_name` and `playlist`:
```
keys:
    ...
    version_name:
        type: str
    playlist:
        type: str
```

Under `strings` in `templates.yml`, add `mno_playlist_job` and `mno_playlist_batch`:
```
strings:
    ...
    mno_playlist_job: 'plyversion_{version_name}'
    mno_playlist_batch: 'playlist_{playlist}'
```



#### Create Playlist Environment
In order for the playlist entity to connect to SGTK, you need to setup an environment file if it does not already exist. Environment files are located under `{SGTKCONFIG}/config/env`. The file must be named to match the Shotgun Studio engine and the entity's name, so `shotgun_playlist.yml`.

If `shotgun_playlist.yml` does not exist use the following:

```
description: This environment controls what items should be shown on the menu in Shotgun
  for shots.
engines:
  tk-shotgun:
    apps:
      tk-shotgun-mno:
        template_mno_config: mno_project_config
        template_work: nuke_shot_work
        deadline_webservice:
          address: phosphene17-pc.nyc.phosphenefx.com
          port: 8082
          platform: win32
          username: webservice
          password: fill-in-password
        sg_write_fields:
          sg_avid: avid
          sg_mno_h264_write: h264
        location:
          path: https://robotAnthony@bitbucket.org/phosfx/tk-shotgun-mno.git
          version: v0.9.1
          type: git
    debug_logging: false
    location:
      version: v0.7.0
      type: app_store
      name: tk-shotgun
frameworks:
include: ./includes/app_launchers.yml
```

After saving the file `shotgun_playlist.yml`, run:
```
tank updates shotgun_playlist
```

#### shotgun_playlist.yml exists

##### Add tk-shotgun to shotgun_playlist env

In the root of your SGTK Project config run the following tank command:
```
tank install_engine shotgun_playlist tk-shotgun
```

##### Install tk-shotgun-mno to the tk-shotgun engine in shotgun_playlist env

YOU **MUST** HAVE **GIT INSTALLED** ON YOUR SYSTEM

Type `git` in your commandline application to ensure you have git installed properly.

If git is installed, run the following command:
```
tank install_app shotgun_playlist tk-shotgun https://robotAnthony@bitbucket.org/phosfx/tk-shotgun-mno.git
```

The latest version of tk-shotgun-mno will be installed.

#### Configure an MNO Submitter for tk-shotgun-mno
MNO requires that all MNO triggering jobs have an `Submitter` PluginInfo Entry. Please select an MNO Submitter name for tk-shotgun-mno so it can properly reference the correct settings in your MNO Config file.

Example using `sgplaylist`:
```
enable: false
# Submitters are the sources of MNO Submissions. This allows for different sources of MNO jobs to use different outputs.
# *Required*
submitters:
    #Name of the submitter. Corresponds to Deadline Job ExtraInfoKey MNO_Submitter
    submitter_name:
    ...
    sgplaylist:
        # Dictionary of Outputs, output's name will be appended to generated scripts and job titles
        # *Required*
        outputs:
            # Descriptive Name for the output
            output_name:
                # Name of Template Path to Nuke Script Template
                script_template: mno_nuke_scrip
                # The Version of Nuke that the script_template is targeted for. Must be #+.#+ i.e. 10.5 not 10.5v2
                # Leverages Deadline's Plugin Settings to Get Actual Path to Nuke Python Executable
                script_nuke_version: 10.5
                # Name of Template Path to Where MNO Will Save The Generated Nuke Script
                mno_template: mno_script_output
                # Name of the read node in the Nuke script that needs output path modified
                # Path assigned to read node comes from original job's output paths
                read: Read1
                # Match against desired write node's sg_path_field setting in order to upload as
                # version thumbnail.
                thumb_path_field: sg_path_to_frames_2
                # Dictionary of Write Nodes, key corresponds to write node's name
                writes:
                    dnxhd:
                        output_template: mno_general_mov_output
                        sg_path_field: sg_path_to_movie
                    h264:
                        output_template: mno_general_mov_output
                        sg_path_field: sg_path_to_h264
                    ...
```

#### Set tk-shotgun-mno app settings in shotgun_playlist.yml

See the `info.yml` file for tk-shotgun-mno.
