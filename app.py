# Copyright (c) 2013 Shotgun Software Inc.
#
# CONFIDENTIAL AND PROPRIETARY
#
# This work is provided "AS IS" and subject to the Shotgun Pipeline Toolkit
# Source Code License included in this distribution package. See LICENSE.
# By accessing, using, copying or modifying this work you indicate your
# agreement to the Shotgun Pipeline Toolkit Source Code License. All rights
# not expressly granted therein are reserved by Shotgun Software Inc.

import sgtk
from sgtk.platform import Application
from sgtk.util.yaml_cache import YamlCache
from sgtk.util import filesystem as fs
from urllib2 import URLError, HTTPError
import traceback
import sys
import os



class MNOOutput(Application):
    """
    The app entry point. This class is responsible for intializing and tearing down
    the application, handle menu registration etc.
    """

    def init_app(self):
        """
        Called as the application is being initialized
        """
        # now register a *command*, which is normally a menu entry of some kind on a Shotgun
        # menu (but it depends on the engine). The engine will manage this command and
        # whenever the user requests the command, it will call out to the callback.

        # first, set up our callback, calling out to a method inside the app module contained
        # in the python folder of the app.
        #
        # Note! By passing the additional (special) entity_type and entity_ids parameters,
        # the Shotgun enging will pass the current selection into the app via those parameters.
        # Also note that this does not work in any other engines.
        #
        deny_permissions = self.get_setting("deny_permissions")
        deny_platforms = self.get_setting("deny_platforms")
        self.logging = sgtk.platform.get_logger(__name__)
        p = {
            "title": "Generate MNO Output",
            "deny_permissions": deny_permissions,
            "deny_platforms": deny_platforms,
            "supports_multiple_selection": True
        }
        self.engine.register_command("mno_output", self.mno_output, p)

    def mno_output(self, entity_type, entity_ids):
        if entity_type.lower() != 'playlist':
            self.logging.error('MNO Output App only works on Playlist entities!')
        else:
            # Get Templates from Settings and Make Sure They Exist!!
            work_template = self.get_template('template_work')
            if not work_template:
                self.logging.error("'{}' path template specified in template_work App Setting could not be found!".format(
                    self.get_setting('template_work')))
                sys.exit(1)
            name_template = self.get_template('template_job_name')
            if not name_template:
                self.logging.error("'{}' string template specified in template_job_name App Setting could not be found!".format(
                    self.get_setting('template_job_name')))
                sys.exit(1)
            batch_template = self.get_template('template_batch_name')
            if not batch_template:
                self.logging.error("'{}' string template specified in template_job_name App Setting could not be found!".format(
                    self.get_setting('template_batch_name')))
                sys.exit(1)

            # Get Deadline Webserivce Settings and a Standalone Deadline Python API Connection
            deadline_settings = self.get_setting('deadline_webservice')
            valid_platforms = ['linux2', 'linux', 'win32', 'cygwin']
            if deadline_settings['platform'] not in valid_platforms:
                self.logging.error("Deadline Webserivce platform setting must be: "+ ', '.join(valid_platforms))
                sys.exit(1)
            try:
                self.deadlinecon = self.get_deadline_connection(
                    deadline_settings['address'], deadline_settings['port'], deadline_settings['username'], deadline_settings['password'])
            except Exception:
                self.logging.error(traceback.format_exc())
                sys.exit(1)
            try:
                repo_root = self.deadlinecon.Repository.GetRootDirectory()
            except URLError as err:
                self.handle_urlerror(err)
            else:
                self.log_separator()
                self.logging.info(
                    "Connecting to Deadline Repository: " + repo_root)
                self.log_separator()

            try:
                if (self.get_setting('deadline_pool')
                    not in self.deadlinecon.Pools.GetPoolNames()):
                    self.logging.error("{} pool not available on Deadline Repository.".format(self.get_setting('deadline_pool')))
                    sys.exit(1)
            except URLError as err:
                self.handle_urlerror(err)

            try:
                mno_outputs = self.get_submitter_outputs(
                    self.get_mno_config_path())
            except MNOConfigError as e:
                self.logging.error(str(e))
                sys.exit(1)

            versions = self.get_playlistversions(entity_ids)
            if versions:
                frames_field = self.get_version_field_name(
                    self.get_setting('sg_path_field'))
                write_fields = self.get_setting('sg_write_fields')
                for version in versions:
                    self.log_separator()
                    self.logging.info("Converting {} Version to Job".format(
                        version[self.get_version_field_name('code')]))
                    self.logging.info("Using {} Playlist Settings".format(
                        version[self.get_playlist_field_name('code')]))
                    try:
                        submission_info = self.version_to_job(
                            version, frames_field, write_fields, name_template, batch_template,
                            deadline_settings['platform'], mno_outputs)
                    except SGVersionError as e:
                        self.logging.error(e)
                        continue
                    except Exception:
                        self.logging.error(traceback.format_exc())
                        sys.exit(1)
                    else:
                        self.logging.info('Submitting Deadline Job')
                        try:
                            job_results = self.deadlinecon.Jobs.SubmitJob(info=submission_info[0], plugin=submission_info[1])
                        except URLError as err:
                            self.handle_urlerror(err)
                        self.logging.info("Deadline Job Submitted Successfully!")
                        self.logging.info('Deadline Batch Name: ' + job_results['Props']['Batch'])
                        self.logging.info('Deadline Job Name: ' + job_results['Props']['Name'])
                        self.logging.info('Deadline Job ID: ' + job_results['_id'])
                    self.log_separator()
                    # self.logging.info(version)
            else:
                self.logging.info('No Playlist Versions Found!')
                sys.exit(0)

    def version_to_job(self, version, frames_field, write_fields, name_template, batch_template, service_platform, mno_outputs):
        #Get the Chosen MNO Output from Playlist Field
        try:
            mno_output_name = version[self.get_playlist_field_name(
                self.get_setting('sg_output_field'))]
            mno_output = mno_outputs[mno_output_name]
        except KeyError:
            err_msg = "Could not find '{}' MNO Output in '{}' MNO Submitter".format(
                version[self.get_playlist_field_name(
                    self.get_setting('sg_output_field'))],
                self.get_setting('mno_submitter')
            )
            raise SGVersionError(err_msg)

        #Gather Validate Selected MNO Writes from Playlist Fields
        valid_writes = []
        for sg_write, mno_write in write_fields.iteritems():
            if version[self.get_playlist_field_name(sg_write)] and mno_write in mno_output['writes']:
                valid_writes.append(mno_write)
        if len(valid_writes) < 1:
            raise SGVersionError("None of the Selected Writes Were Found in '{}' MNO Output".format(mno_output_name))

        #Build Out Paths for Work File from Image Sequence Path
        frames_template = self.get_template('template_frames')
        if not frames_template:
            raise SGVersionError(
                "Could not find '{}' Template Path for setting template_frames".format(self.get_setting('template_frames')))
        template_fields = frames_template.validate_and_get_fields(version[frames_field])
        self.logging.debug("Path in {}: {}".format(self.get_setting('sg_path_field'), version[frames_field]))
        self.logging.debug("Fields from {} field:".format(self.get_setting('sg_path_field')))
        self.logging.debug(str(template_fields))
        #Path in frames_field does not match template_frames. Try changing the root as it may be from a different platform.
        if not template_fields:
            self.logging.debug("Path in {} field does not match template, attempting to switch roots.".format(self.get_setting('sg_path_field')))
            platform_roots = dict(frames_template._per_platform_roots)
            original_root = platform_roots.pop(sys.platform, None)
            for root in platform_roots:
                if platform_roots[root]:
                    translated_path = os.path.normpath(version[frames_field].replace(platform_roots[root], original_root))
                    template_fields = frames_template.validate_and_get_fields(translated_path)
                    if template_fields:
                        break
            if not template_fields:
                raise SGVersionError("Could not match path in '{}' SG Field to '{}' Template path.".format(self.get_setting('sg_path_field'), self.get_setting('template_frames')))
        template_fields['playlist'] = version[self.get_playlist_field_name(
            'code')]
        template_fields['version_name'] = version[self.get_version_field_name(
            'code')]

        job_output = frames_template.apply_fields(template_fields, platform = service_platform)
        self.logging.debug("Output Path for Deadline Job: " + job_output[0] + job_output[1])
        #Construct Deadline Job Info
        job_info = {
            'Name': name_template.apply_fields(template_fields),
            'BatchName': batch_template.apply_fields(template_fields),
            'Frames': version[self.get_version_field_name('frame_range')],
            'OverrideTaskExtraInfoNames': 'False',
            'Plugin': 'MNO',
            'ExtraInfo5': 'playlistToMNO',
            'ChunkSize': 100000,
            'Pool': self.get_setting('deadline_pool')
        }
        extra_info = {
            'QTLUT': 'FALSE',
            'MNO': 'FALSE',
            'VersionId': str(version[self.get_version_field_name('id')]),
            'TaskId': 'playlistToMNO',
            'SGSkipStatus': 'TRUE',
            'UserName': 'tk-shotgun-mno'
        }
        add_extra_info_keys(job_info, extra_info)

        #Construct Deadline Job Plugin Info
        plugin_info = {
            'Outputs': mno_output_name,
            'Submitter': self.get_setting('mno_submitter'),
            'Writes': ';'.join(valid_writes),
            'ImgSeq': job_output
        }

        return (job_info, plugin_info)

    def get_playlistversions(self, playlist_ids):
        version_fields = self.get_version_field_names(
            ['id', 'code', 'frame_range', self.get_setting('sg_path_field')]
        )
        fields = self.get_playlist_field_names(
            ['id', 'code', self.get_setting(
                'sg_output_field')] + self.get_write_fields()
        )
        fields.extend(version_fields)
        filters = [['playlist.Playlist.id', 'in', playlist_ids]]
        versions = self.shotgun.find(
            "PlaylistVersionConnection", filters, fields)

        return versions

    def get_write_fields(self):
        return self.get_setting("sg_write_fields").keys()

    def get_connection_field_name(self, prefix, field):
        return prefix + field

    def get_playlist_field_name(self, field):
        return self.get_connection_field_name('playlist.Playlist.', field)

    def get_playlist_field_names(self, fields):
        return [self.get_playlist_field_name(field) for field in fields]

    def get_version_field_name(self, field):
        return self.get_connection_field_name('version.Version.', field)

    def get_version_field_names(self, fields):
        return [self.get_version_field_name(field) for field in fields]

    def get_mno_config_path(self):
        try:
            return self.sgtk.paths_from_template(self.get_template('template_mno_config'), {}, skip_keys=['project'])[0]
        except IndexError:
            raise MNOConfigError("Could not find a path using '{}' path template key!".format(
                self.get_setting('template_mno_config')))

    def handle_urlerror(self, err):
        if type(err).__name__ == 'HTTPError':
            if err.code == 401:
                error_msg = """Error: HTTP Status Code 401. Authentication with the Web Service failed.
                    Please ensure that the authentication credentials are set, are correct, and that authentication mode is enabled."""
            else:
                error_msg = err
        else:
            error_msg = "Could not find Deadline Webserivce at {}:{}".format(
                deadline_settings['address'], deadline_settings['port'])
        self.logging.error(error_msg)
        sys.exit(1)

    def get_submitter_outputs(self, mno_config_path):
        if not mno_config_path:
            raise MNOConfigError(
                "Could not find MNO Config File specified by template_mno_config setting!")

        yamlcache_obj = YamlCache()
        mno_config = yamlcache_obj.get(mno_config_path)
        mno_submitter = self.get_setting('mno_submitter')
        try:
            if mno_config['enable'] == False:
                raise MNOConfigError(
                    'MNO has been disabled in the MNO Config file!')
        except KeyError:
            raise MNOConfigError(
                'MNO Config file is malformed, missing enable key!')
        try:
            return mno_config['submitters'][mno_submitter]['outputs']
        except KeyError:
            raise MNOConfigError(
                "Could not find MNO Submitter '{}' in MNO Config file.".format(mno_submitter))

    def log_separator(self, sep_len=10, sep='-'):
        self.logging.info('')
        self.logging.info(sep * sep_len)
        self.logging.info('')

    def get_deadline_connection(self, address, port, username=None, password=None):
        sys.path.append(os.path.join(self.disk_location, 'python'))
        try:
            from DeadlineAPI.DeadlineConnect import DeadlineCon
        except ImportError:
            self.logging.error('Could not import DeadlineAPI')
            self.logging.error(traceback.format_exc())
            sys.exit(1)
        else:
            deadlinecon = DeadlineCon(address, port)
            if username and password:
                deadlinecon.SetAuthenticationCredentials(username, password)
            return deadlinecon

def add_extra_info_keys(job_info, extra_info_dict):
    kv_index = 0
    for k, v in extra_info_dict.iteritems():
        job_info["ExtraInfoKeyValue" + str(kv_index)] = k + '=' + v
        kv_index += 1

def check_for_mno_event_error(error_reports):
    for report in error_reports:
        if report['Plugin'] == 'MNO':
            return True
    return False

class MNOConfigError(Exception):
    pass


class SGVersionError(Exception):
    pass
